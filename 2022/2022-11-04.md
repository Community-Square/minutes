## 2022-11-04

### Agenda

- Codeberg Pages Content
- Logo and Name
- Matrix-Raum
- FOSS Backstage CfP (<https://23.foss-backstage.de/>)
- What does a community manager do? (What are the responsibilities and which aren't)
- Why is a community manager important? Why does it need a responsible person for community management?

### Codeberg Pages Content

- Use text from CfP for the main page
- add a link to VideoConference
- add link to Codeberg organisation
- add link to Matrix
- add link to the minutes
- Jan proposes to create a mastodon account
- Jan proposes a mastodon server
- License: articles Creative Commons CC-BY-SA, Code: List each license of each component, use MIT license (or whatever license is usually used in the Hugo project for code)

### Logo and Name

- Do we want a logo and a text/claim?
- Logo: No color gradient
- Logo proposal: molecules and networks, etc.
- color proposal: lilac/purple
- word mark, picture mark + catchphrase, word mark = Community², catchphrase: pick up OS & community management
- Ariane talks to Mike and brings him along to present his proposals

### Matrix-Room

- greet new visitors/people joining the room
- anybody can do this
- admin rights to the group initiators

### FOSS Backstage CfP (<https://23.foss-backstage.de/>)

- Eduard will hand in a proposal for participation
- Everybody in attendance should receive a mail from preTalks

### What does a community manager do? (What are the responsibilities and which aren't)

**Do's**

- Knows the right persons in the community to help others
- Resolve impediments for contributors (e.g. pending reviews, missing communication, etc.)
- Be present, be available for questions, "first level support"
- Visit relevant conferences, events and "be a face of the project"
- You're only as successful as your community, so be grateful of the shoulders you're standing on!
- Be a part of your community! Try not to be perceived as something foreign.
- Events
- Work transparent, include the opinion of your community and let them discuss or contribute openly to your plans!
- Foster transparency in the community
- Encourage and help others (e.g. by giving advices or worth-reading tips) to share lessons, create content and "spread the word"
- Developer Advocacy: Keep care of your contributors, leverage a welcoming and faithful environment for the community
- Take action if the community is in danger of collapsing or breaking
- Keep the community informed and engaged (e.g. by newsletter, social media, etc.)

**Dont's**

- Actively contribute code to the project
- Resolve individual conflicts (-> in the scope of Code of Conduct boards)
- Marketing
- Social Media marketing
- Don't try to be the central figure of your project (You're not Elon Musk, dude!)

### Backlog identified during meeting:

- How can I start community management from scratch / How to bootstrap community management?
- Write a good summary on how to deal with conflicts (both individual and wider)
- What are the differences between Developer Advocacy and Community Manager
- Mental Health, Inclusion, Diversity
- What is a good time allocation for a community manager?
- Why is a community manager important? Why does it need a responsible person for community management?

- Action item: Create a first document draft on the role of a community manager

### Why is a community manager important? Why does it need a responsible person for community management?

Moved to backlog

### Proposed agenda for the next meeting

- Why is a community manager important? Why does it need a responsible person for community management?

### Attendance

- Ariane
- Eduard
- Jan
- Max
- Otto
