## 2022-11-18

### Agenda

- Many new people in the chat
- Corporate Identity <3
- FOSS Backstage Sponsorship
- Housekeeping (Pull Requests)
- Why is a community manager important? Why does it need a responsible person for community management?

### Many new people in the chat

* Have a look at https://coding.social/ and try to be more verbose in the chat, i.e. asynchronously discuss worth-reading material

### Corporate Identity <3

* Ariane presents the proposed CI.
* Proposed logo: A mushroom (as an integral part of the "Wood Wide Web")
* Add a little network/networking into the log as well?
  A little mystic atmosphere? Moonshine?
  Mushrooms at the surface collect knowledge, tie the community together (nutrients = Knowledge et al.)
  color palette in the middle is exciting, but little contrast -> Mike will continue from there
  Font: Quicksand

### FOSS Backstage Sponsorship

* If you're interested in supporting the FOSS Backstage conference next year, please reach out to Paul ([@thePB:matrix.org](https://matrix.to/#/@thePB:matrix.org)). Various sponsorship packages are available!

### Housekeeping (Pull Requests)

* Create a CI to build the Hugo-based static page
* Pull requests are merged by any owner after a successful review
* Keep minutes in one single file (as long as the site renders in a reasonable time)
* Start to work on the document on the role of a CM (https://codeberg.org/Community-Square/library/issues/1)
* Ask the people in our Matrix chat to add worth-reading assets to the library (https://codeberg.org/Community-Square/library)

### Why is a community manager important? Why does it need a responsible person for community management?

Value proposition of a Community Manager

- Developers (and especially maintainers) have a huge workload
- Keeping care of community management topics usually have a low priority due to the workload
- However: Wishes for community activities are present in developers' heads!
- In terms of events, it is important to not "bind" contributors (which may want to follow the event) with organizational topics
- Community Management (and communication) may not be everyone's beloved activity
  -> you can't force it onto anyone!
- [Diffusion of responsibility](https://en.wikipedia.org/wiki/Diffusion_of_responsibility)
- There needs to be a dedicated person to take care of houskeeping
- A "neutral" person (that isn't involved in the internal/technical discussions) can help to push things forward and keep the team ongoing
- Keeps tracks on the overall vision and Community goals (and helps/pushes to create them in the first place!) and makes sure that the community is working towards those Community goals, tracks milestones etc
  -> Example: "Our goal to have a technical roadmap!" -> Community Manager's job is not to build that roadmap on their own, but make sure that it's created by the responsible people
- There needs to be a dedicated persons that keeps care of soft "Man müsste mal und man könnte mal...^TM" tasks
- Outreach and networking is very important for Open Source projects.

Question: Is community manager a german/european thing? -> check out!

### Proposed agenda for the next meeting

- "Hobby projects" vs. Open Source projects with a business case: What's the role of a community manager, where's the difference? Why do have some huge projects no dedicated Community Manager role (e.g. Linux Kernel). In which context does this role make sence?

### Attendance

- Katharina
- Eduard
- Jan
- Max
- Ariane
