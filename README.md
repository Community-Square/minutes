# Minutes of Community² gatherings

We try to record some of the ideas during the meetings at
<https://input.scs.community/Community%C2%B2_minutes> and archive them in this
repository.

- 2024
  - [Jan 26, 2024](2024/2024-01-26.md)
- 2023
  - [Nov 24, 2023](2023/2023-11-24.md)
  - [Oct 27, 2023](2023/2023-10-27.md)
  - [Oct 13, 2023](2023/2023-10-13.md)
  - [Sep 29, 2023](2023/2023-09-29.md)
  - [Jul 21, 2023](2023/2023-07-21.md)
  - [May 26, 2023](2023/2023-05-26.md)
  - [Apr 14, 2023](2023/2023-04-14.md)
  - [Mar 31, 2023](2023/2023-03-31.md)
  - [Feb 10, 2023](2023/2023-02-10.md)
- 2022
  - [Dec 02, 2022](2022/2022-12-02.md)
  - [Nov 18, 2022](2022/2022-11-18.md)
  - [Nov 04, 2022](2022/2022-11-04.md)
  - [Oct 21, 2022](2022/2022-10-21.md)

## Current Backlog

- FOSS Backstage 2024 - Anyone going to participate, despite our event being not accepted?
- codeberg account creation & introduction
- "Hobby projects" vs. Open Source projects with a business case: What's the
  role of a community manager, where's the difference? Why do have some huge
  projects no dedicated Community Manager role (e.g. Linux Kernel). In which
  context does this role make sense?
- How can I start community management from scratch / How to bootstrap community
  management?
- Write a good summary on how to deal with conflicts (both individual and wider)
- What are the differences between Developer Advocacy and Community Manager
- Mental Health, Inclusion, Diversity
- What is a good time allocation for a community manager?
- Why is a community manager important? Why does it need a responsible person
  for community management?
- How to gather feedback
